<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



use App\Task;
// get all tasks.
Route::get('tasks', 'TaskController@index');

// create new task.
Route::post('tasks', 'TaskController@store');

// delete task with id.
Route::delete('tasks/{id}', 'TaskController@delete');

// update task with id.
Route::put('tasks/{id}', 'TaskController@update');

// get task with id.
Route::get('tasks/{id}', 'TaskController@show');
