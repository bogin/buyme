<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Deploy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Building database...');
        $this->call('migrate');

        $this->comment('Seeding database...');
        $this->call('db:seed');

        $this->comment('Clearing config cache...');
        $this->call('config:clear');

        $this->comment('Clearing cache...');
        $this->call('cache:clear');

        $this->comment('Clearing compiled...');
        $this->call('clear-compiled');

        $this->comment('Clearing and Rebuilding route cache...');
        $this->call('route:cache');

        $this->comment('Clearing and Rebuilding configuration cache...');
        $this->call('config:cache');

        $this->comment('Clearing view cache...');
        $this->call('view:clear');

        $this->comment('Dumping autoload files...');
        exec('composer dump-autoload');

        $this->comment('All done! :)');
    }
}
