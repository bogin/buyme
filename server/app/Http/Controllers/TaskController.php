<?php

namespace App\Http\Controllers;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Log;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
      * return format {"tasks":[
      * {"id":1,"description":"asdasd","deadline":"asda","isDone":1},
      * {"id":2,"description":"ascasc","deadline":"asasc","isDone":0},
      * {"id":3,"description":"sddsc","deadline":"sdsds","isDone":0}
      * ]}
     */
    public function index()
    {
        $tasks = Task::all();
        return response()->json(['tasks' => $tasks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * return format {task: {description: "sddsc", deadline: "sdsds", isDone: false}}
     */
    public function store(Request $request)
    {
        $input = $request->input('task');

        $data = array(
              'description' => $input['description'],
              'deadline' => $input['deadline'],
              'isDone' => $input['isDone']
            );

        $task = Task::create($data);
        return response()->json(['task' => $task]);
    }

    /**
     * Delete the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        return response()->json(204);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * return format {task: {description: "sddsc", deadline: "sdsds", isDone: false}}
     */
    public function update(Request $request, $id)
    {
      $task = Task::findOrFail($id);
      $task->description = $request->input('task')['description'];
      $task->deadline = $request->input('task')['deadline'];
      $task->isDone = $request->input('task')['isDone'];
      $task->update();
      return response()->json(['task' => $task]);
    }

    /**
     * get resource by id from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * return format {task: {description: "sddsc", deadline: "sdsds", isDone: false}}
     */
    public function show($id)
    {
        $task = Task::find($id);
        return response()->json(['task' => $task]);
    }




}
