<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel CORS
    |--------------------------------------------------------------------------
    |
    | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
    | to accept any value.
    |
    */

      'supportsCredentials' => false,
      'allowedOrigins' => [ '*', 'http://localhost:4200'],
      'allowedHeaders' => ['*'],//Content-Type', 'application/json
      'allowedMethods' => ['*'], // ex: ['GET', 'POST', 'PUT',  'DELETE']
      'exposedHeaders' => ['Authorization'],
      'maxAge' => 0,
];
