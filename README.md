# Ember and Laravel 5.6 With Homestead and Vagrant - To Do List :memo:

This is a simple Ember application that uses:
* [ember-cli-mirage](https://github.com/samselikoff/ember-cli-mirage)
* [ember-one-way-controls](https://github.com/DockYard/ember-one-way-controls)
* [ember-route-action-helper](https://github.com/DockYard/ember-route-action-helper)


## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://ember-cli.com/)
* [Homestead](https://laravel.com/docs/5.6/homestead)
* [Vagrant](https://www.vagrantup.com/)

I'm developing on windows 10.
## Client Installation

* ` git clone <repository-url> ` this repository.
*  Change into the new directory into frontend folder.
* ` npm install `.
* ` bower install `.

## Running / Development

* ` ember server `.
* Visit your app at [http://localhost:4200](http://localhost:4200).

## Server Installation

*  Download vagrant from here https://www.vagrantup.com/downloads.html
*  Install GitBash.
*  Run in GitBash ` vagrant box add laravel/homestead `.
*  In GitBash go to cd home.
*  Run ` git clone https://github.com/laravel/homestead.git Homestead `.
*  It will clone Homestead repository from GitHub to C:\Users\USER_NAME directory
   where USER_NAME is your username.
*  Now run the following commands:
*  ` cd Homestead `
*  ` bash init.sh `
*  This command will create Homestead.yaml configuration file
   which will be placed in ` C:\Users\USER_NAME\Homestead ` directory.
   replace it with my file.
*  Now we need to create ssh keys. Go to ` C:\Users\USER_NAME ` directory
   and find out the folder named .ssh. If the folder exists,
   navigate into the folder and see if two files named id_rsa and id_rsa.pub are present.
   If they are present, continue to " Add this to C:/Windows.." right bellow.
   If the folder .ssh does not exist or the folder is present but the two files are not present.
   Run the following command from GitBash. Make sure to replace
   my_email@example.com with your email address in the command below..

   ` ssh-keygen -t rsa -C “my_email@example.com” `

   Just press enter whatever command line asks you. It will ask you two things, simply press enter.
   It will create a folder named .ssh with two files named id_rsa and id_rsa.pub in it.

*  Add ` 192.168.10.10 alpha.test ` to ` C:/Windows/System32/Drivers/etc/hosts file `.
*  In GitBash go to ` C:\Users\USER_NAME\Homestead `.
*  Run ` vagrant up `.
*  Run ` vagrant ssh `.
*  Run ` cd code/server/`.
*  Run ` php artisan re:all `.

 If its all go well then u r good !


# About the Server API

If you would like to test the server via external HTTP tool like Postman,
you can use the API bellow.
please make sure that the request has the header:
  ` Content-Type', 'application/json `

##   GET http://server.test/api/tasks

Get all tasks.

Return format:

   {  "tasks": [
              {
                  "id": 1,
                  "description": "Do That",
                  "deadline": "Some Date",
                  "isDone": 1
              },
              {
                  "id": 2,
                  "description": "Go There",
                  "deadline": "10.08.2018",
                  "isDone": 0
              },
              {
                  "id": 3,
                  "description": "Wedding",
                  "deadline": "Oct 21",
                  "isDone": 0
              }
          ]
      }


##  POST http://server.test/api/tasks

Create new task.

Expected input:


    {  "tasks": {
            "description": "Don't Forget Me",
            "deadline": "Oct 20",
            "isDone": false
            }
          }


Return format:

    {  "tasks": {
            "description": "Dont Forget Me",
            "deadline": "Oct 20",
            "isDone": false,
            "id": 4
          }
        }



##  PUT http://server.test/api/tasks/:id

Update task.

Expected input:

     {  "task": {
    				"description": "Dont Forget Me",
    			    "deadline": "Oct 20",
    			    "isDone": false
    			}
        }


Return format:

     {  "task": {
              "id":1,
              "description":"Dont Forget Me",
              "deadline":"Oct 20",
              "isDone":false
          }
        }


##  GET http://server.test/api/tasks/:id

Get one task.


Return format:

     {  "task": {
              "id":1,
              "description":"Dont Forget Me",
              "deadline":"Oct 20",
              "isDone":false
          }
        }




## Useful Commands For Vagrant That I Added

While in folder ` code/server ` the following commands has proving to
be useful for developing:

 * ` re:cache ` - Clears all the cache - like reset routes.
 * ` re:db ` - Clear the db.
 * ` re:all ` - Does the two above and a deploy.


## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://ember-cli.com/)
* [ember-tutorial](https://github.com/ssriera/ember-to-do-list)
* [Laravel tutorial]
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
