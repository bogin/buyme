import DS from "ember-data";


//RESTAdapter for latravel 
export default DS.RESTAdapter.extend({
  host: 'http://server.test',
  namespace: 'api',
  headers: {
        accept: 'application/json',
        "Content-Type": 'application/json',
    },
});
