import Ember from 'ember';

export default Ember.Controller.extend({
  isDone: false,

  current: '',

  completed: Ember.computed( 'model', 'current', function() {

    // task model
    var tasks = this.get('model');

    // user decision what to display
    var current = this.get('current');


    // choose what to display - all , completed or incomplete
    let finished = current === 'complete';
    let unfinished = current === 'incomplete';

    // show all completed
    if (finished) {
      return tasks.filter((task) => {
        return task.get('isDone') === true;
      });
    }

    // show all incompleted
    if (unfinished) {
      return tasks.filter((task) => {
        return task.get('isDone') === false;
      });
    }

    // return tasks to display
    return tasks;
  }),

  actions: {
    // firest 3 function here controll the code above
    // by setting the current virable
    showAll() {
      this.set('current', 'all');
    },
    showCompleted() {
      this.set('current', 'complete');
    },
    showIncomplete() {
      this.set('current', 'incomplete');
    },

    // route to /edit/:id for editing
    editTask(task , model) {
      this.transitionToRoute('edit', task.get('id'));
    }
  }
});
