import Ember from 'ember';

export default Ember.Controller.extend({
  // bind alias .
  task: Ember.computed.alias('model'),

  actions: {
    // finish editing.
    saveEdit(model, description, deadline) {

      //get model
      let todo = this.get('model');

      //set the model new properties.
      todo.setProperties( {
        description,
        deadline
      });

      // send update request to server.
      todo.save().then(() => {

        // get back to main page without editing.
        this.transitionToRoute('application');

        // reset model properties for the next edit.
        this.set('description',undefined);
        this.set('deadline',undefined);
      });

    }
  }
});
