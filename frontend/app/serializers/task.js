
import DS from 'ember-data';

export default DS.RESTSerializer.extend({

normalizePayload: function(payload) {
    return {
      // create ember format out of payload.
        'task': {
            id: payload.id,
            description: payload.description,
            deadline: payload.deadline,
            isDone: payload.isDone
        }
    };
  }
});
