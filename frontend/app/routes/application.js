import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.findAll('task');
  },

  actions: {

    // create new task.
    save(description, deadline) {

      // create client record.
      this.get('store').createRecord('task', {
        description,
        deadline
      }).save(); // send create request to server


      // reset inputs fields in application template
      this.controller.set('deadline', '');
      this.controller.set('description', '');
    },

    // delete.
    delete(task) {

      // delete from client records.
      task.deleteRecord();

      // send delete request to server
      task.save();
    },

    // mark as complate.
    toggleTask(task) {

      // get ture or false if done(complete).
      let isDone = task.get('isDone');

      // update client record.
      task.set('isDone', !isDone);

      // send update request to server 
      task.save();
    }
  }
});
